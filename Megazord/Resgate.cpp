#include "Resgate.h"

Resgate::Resgate(){
}

void Resgate::iniciar (){
	  this -> entrarNaSala ();
    char zona = this -> identificarZona();
    if (zona != 'c'){
        this -> voltar (zona);
    }
    this -> seguirNaSala (zona);
} 


// método responsável por verificar se a area de resgate
// está na frente do robô ou não
bool Resgate::verificarArea (){
    robo.acionarMotores (0,0);
    delay (1000);
    if (robo.lerSensorSonarEsq () <= 20){
    	  for(int i = 0; i < 10; i++){  
            robo.ligarTodosLeds();
            delay(400);
            robo.desligarTodosLeds();
            delay(200);
        }
        return true;
    }else {
      	return false;
    }
}



// construimos um método responsável por alinha o robô
// com a parede atrás dele, dessa forma reduzimos a força dos motores
// e aumentamos a práticidade com que ele alinha com a parede 
void Resgate::alinharRobo (){
    robo.acionarMotores (-40,-38);
    delay (1000);
    robo.acionarMotores (40,38);
    delay (200);
    robo.acionarMotores (-40,-38);
    delay (1000);
    robo.acionarMotores (40,38);
    delay (150);
    robo.acionarMotores (-40,-38);
    delay (1000);
    robo.acionarMotores (0,0);
    delay (1000);
}

// faz com que o robô entre na sala de forma paralela
// assim facilitara o seu alinhamento com a parede
void Resgate::entrarNaSala (){
    robo.acionarMotores(60,20);
    delay (520);
    robo.acionarMotores (-40,-38);
    delay (250);
    
    while (robo.lerSensorSonarEsq() < 20){
        robo.acionarMotores (40,-38);
        delay (50);
    }
    robo.acionarMotores (40,-38);
    delay (280);
    this -> alinharRobo ();
}



// nesse método iremos fazer com que o robô encontre a sala 
// onde deve ser deixado a bolinha

char Resgate::identificarZona (){
    robo.desligarTodosLeds();
    robo.acionarMotores (-40,38);
    delay (550);
    robo.acionarMotores (0,0);
    delay (550); 
    garra.baixar ();
    garra.abrir ();
    robo.acionarMotores (0,0);
    delay (1000);


    
    // seguir um pouco 
    robo.acionarMotores (40,38);
    delay (1000);
    robo.acionarMotores (0,0);
    delay (550);
    garra.fechar ();
    garra.subir (); 
    robo.acionarMotores (0,0);
    delay (2000);
    robo.acionarMotores (40,-38);
    delay (230);
    robo.acionarMotores (40,38);
    delay (900);
    robo.acionarMotores (0,0);
    delay (3000);
    
    if (verificarArea()){
        robo.acionarMotores (-40,38);
        delay (550);
        robo.acionarMotores (40,38);
        delay (150);
        robo.acionarMotores (0,0);
        delay (3000);
        robo.acionarServoGarra2 (30);
        delay (1000);
        robo.acionarServoGarra1 (180);
        delay (2000);  
        robo.ligarLed(1);
        return 'a';
    }

    mover.girarParaDireita();
    delay (225);
    robo.acionarMotores (0,0);
    delay (1000);

    this-> alinharRobo ();  
    robo.acionarMotores (0,0);
    delay (1000);

    garra.baixar ();
    garra.abrir ();
    robo.acionarMotores (0,0);
    delay (1200);
    
    // seguir um pouco 
    robo.acionarMotores (40,38);
    delay (1200);
    robo.acionarMotores (0,0);
    delay (1000);
    garra.fechar ();
    garra.subir (); 
    robo.acionarMotores (0,0);
    delay (2000);
    robo.acionarMotores (40,-38);
    delay (230);
    robo.acionarMotores (40,38);
    delay (900);
    robo.acionarMotores (0,0);
    delay (3000);
  
    if (verificarArea()){
        robo.acionarMotores (-40,38);
        delay (550);
        robo.acionarMotores (40,38);
        delay (150);
        robo.acionarMotores (0,0);
        delay (3000);
        robo.acionarServoGarra2 (30);
        delay (1000);
        robo.acionarServoGarra1 (180);
        delay (2000);  
        robo.ligarLed(2);
        return 'b';
    }

    robo.acionarMotores (-40,-38);
    delay (900);
    robo.acionarMotores (-40,38);
    delay (230);
    robo.acionarMotores (-40,-38);
    delay (1200);
    this -> alinharRobo ();

    robo.ligarLed(3);
    return 'c';
}

// construimos um método voltar para cada vez que 
// o robô estiver na area de resgate ele irá volta ao ponto 0 
void Resgate::voltar (char zona){
    garra.subir ();
    garra.fechar ();
    robo.acionarMotores(-40,-38);
    delay (800);
  
    switch (zona){
        case 'a':           
            mover.girarParaDireita();
            delay (225);
            robo.acionarMotores(-40,-38);
            delay (2000);
            this -> alinharRobo ();

            robo.acionarMotores(40,38);
            delay (100);
            mover.girarParaDireita();
            delay (550);
            robo.acionarMotores(-40,-38);
            delay (1000);
            this -> alinharRobo ();
         
        case 'b':       
            mover.girarParaEsquerda ();
            delay (225);
            robo.acionarMotores(-40,-38);
            delay (2000);
            this -> alinharRobo ();
            
            robo.acionarMotores(40,38);
            delay (100);
            mover.girarParaDireita();
            delay (550);
            robo.acionarMotores(-40,-38);
            delay (1000);
            this -> alinharRobo ();
            
        default:    
            mover.girarParaDireita();
            delay (225);
            robo.acionarMotores(-40,-38);
            delay (2000);
            this -> alinharRobo();

            robo.acionarMotores(40,38);
            delay (100);
            mover.girarParaEsquerda();
            delay (550);
            robo.acionarMotores(-40,-38);
            delay (1000);
            this -> alinharRobo ();            
    }
}
        
void Resgate::irLateralEsq (){
    robo.acionarMotores (-40,38);
    delay (550);
    robo.acionarMotores (0,0);
    delay (1000);
    garra.abrir ();
    garra.baixar ();
    robo.acionarMotores (0,0);
    delay (1000);

    robo.acionarMotores (40,38);
    delay (1000);
    robo.acionarMotores (0,0);
    delay (1000);   
    garra.fechar ();
    garra.subir ();
    robo.acionarMotores (0,0);
    delay (1000);   
    
    robo.acionarMotores (-40,38);
    delay (550);
}



void Resgate::irLateralDir (){
    robo.acionarMotores (40,-38);
    delay (550);
    robo.acionarMotores (0,0);
    delay (1000);
   
    garra.abrir ();
    garra.baixar ();
    robo.acionarMotores (0,0);
    delay (1000);

    robo.acionarMotores (40,38);
    delay (1000);
    robo.acionarMotores (0,0);
    delay (1000);   
    garra.fechar ();
    garra.subir ();
    robo.acionarMotores (0,0);
    delay (1000);   
    
    robo.acionarMotores (-40,38);
    delay (550);
}



// método responsável por deixar a bolinha na determinada zona de resgate
// ao passarmos um char ('a'/'b'/'c') iremos ter noção de qual area vai se tratar 
// logo após a entrega da bolinha, o nosso robô deve voltar para o lugar de origem

void Resgate::areaResgate (char zona){
    // primeiramente vamos pegar a bolinha que está a frente do robô
    // depois vamos alinha com a parede à esquerda da sala   
    robo.acionarServoGarra2 (0);
    delay (1000);       
    robo.acionarServoGarra1 (180);
    delay (1000);       
    robo.acionarServoGarra1 (90);
    delay (1000);       
    robo.acionarServoGarra2 (100);
    delay (1000);       
    robo.acionarMotores (40,38);
    delay (1200);


    if (zona == 'c'){
        robo.acionarMotores(40,-38);
        delay (550);    
        robo.acionarMotores (0,0);
        delay (2000);   
    }else {
        robo.acionarMotores(-40,38);
        delay (550);    
        robo.acionarMotores (0,0);
        delay (2000);   
    }



    this -> alinharRobo ();       
    robo.acionarMotores(40,38);
    delay (1800);
    robo.acionarMotores (0,0);
    delay (2000);   

    if (zona == 'a'){     
        robo.acionarMotores(40,-38);
        delay (775);
        robo.acionarMotores(-40,-38);
        delay (500);
        this -> alinharRobo ();

        // giro para ficar de frente com a sala
        robo.acionarMotores(40,-38);
        delay (1100);
        robo.acionarMotores(40,38);
        delay (150);

        // soltar bolinha na area 
        robo.acionarServoGarra2 (30);
        delay (1000);
        robo.acionarServoGarra1 (180);
        delay (2000);
    }   
    
    else if (zona == 'b'){
        robo.acionarMotores(-40,38);
        delay (775);
        robo.acionarMotores(-40,-38);
        delay (500);
        this -> alinharRobo ();

        // giro para ficar de frente com a sala
        robo.acionarMotores(40,-38);
        delay (1100);
        robo.acionarMotores(40,38);
        delay (150);

        // soltar bolinha na area 
        robo.acionarServoGarra2 (30);
        delay (1000);
        robo.acionarServoGarra1 (180);
        delay (2000);
    }
    
    else {
        robo.acionarMotores(40,-38);
        delay (775);
        robo.acionarMotores(-40,-38);
        delay (500);
        this -> alinharRobo ();      

        // giro para ficar de frente com a sala
        robo.acionarMotores(40,-38);
        delay (1100);
        robo.acionarMotores(40,38);
        delay (150);

        // soltar bolinha na area 
        robo.acionarServoGarra2 (30);
        delay (1000);
        robo.acionarServoGarra1 (180);
        delay (2000);         
    }
}




// método super importante, pois por meio dele 
// é possivel pegar a bolinha e deixar em sua determinada area
void Resgate::seguirNaSala (char zona){
    for (int i = 0; i < 3; i++){
        garra.baixar();
        garra.abrir();
        robo.acionarMotores(40,-38);
        delay (2000);
        
        robo.acionarMotores(40,38);
        delay (1800);
        garra.fechar();
        delay (1000);
        robo.acionarMotores(-40,-38);
        delay (1800);
        this -> alinharRobo ();
 
        robo.acionarServoGarra1 (90,170, 40);
        garra.subir();
        delay(2000);

        if (robo.lerSensorSonarFrontal() < 20){
            this -> areaResgate (zona);
            this -> voltar(zona);
            i = 0;
        }
        else if (zona == 'a' or zona == 'b'){
            this-> irLateralEsq ();   
        }else {
            this-> irLateralDir ();
        }
    }
}
        
