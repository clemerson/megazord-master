#ifndef RESGATE_H
#define RESGATE_H

#include <robo_hardware2.h>
#include "Movimento.h"
#include "Garra.h"

class Resgate: public Garra {
    private:
      	Garra garra;
        Movimento mover;

    protected:
    	  // método mais importante
        void seguirNaSala (char zona);
         
      	void irLateralDir ();
      	void irLateralEsq ();

      	void alinharRobo ();    	
        void entrarNaSala ();
        char identificarZona (); 
		    bool verificarArea ();
        void voltar (char zona);
        void areaResgate (char zona);
    public:
        Resgate ();	
        void iniciar ();
};


#endif
